### Mino test service

Node.js service to test Elastic Beanstalk and DynamoDb

### Local

Run docker-compose up to launch the application

API documentation will be available at http://127.0.0.1:8081/docs

### Tests

Run ```npm run test:int``` to run the integration scenario
Run ```npm test``` to run the unit tests

### Linting

Linting uses standard js [StandardJs](https://standardjs.com/).

Run npm lint to view the linting errors in your code

### CI / CD

Uses gitlab-ci to test, build and deploy.

You have to configure the following values in your gilab-ci variables:
- DYNAMO_ACCESS_KEY_ID: AWS access key ID with Dynamo privileges only
- DYNAMO_SECRET_ACCESS_KEY: AWS secret access key with Dynamo privileges only
- DYNAMO_ENDPOINT: Dynamo endpoint ex: http://elasticbeanstalk.us-east-1.amazonaws.com
- AWS_ACCESS_KEY_ID: Admin key used to manipulate Elastic Beanstalk and DynamoDb
- AWS_SECRET_ACCESS_KEY: Admin secret access key used to manipulate Elastic Beanstalk and DynamoDb

### Database design

#### User table
{
  TableName: 'users',
  KeySchema: [
    { AttributeName: 'id', KeyType: 'HASH' } // Partition key
  ],
  AttributeDefinitions: [
    { AttributeName: 'id', AttributeType: 'S' },
    { AttributeName: 'groupId', AttributeType: 'S' }
  ],
  GlobalSecondaryIndexes: [{
    IndexName: 'groupId',
    KeySchema: [
      {
        AttributeName: 'groupId',
        KeyType: 'HASH'
      }
    ],
    Projection: {
      ProjectionType: 'ALL'
    },
    ProvisionedThroughput: {
      ReadCapacityUnits: 5,
      WriteCapacityUnits: 5
    }
  }],
  ProvisionedThroughput: {
    ReadCapacityUnits: 5,
    WriteCapacityUnits: 5
  }
}

#### Groups table

{
  TableName: 'groups',
  KeySchema: [
    { AttributeName: 'id', KeyType: 'HASH' } // Partition key
  ],
  AttributeDefinitions: [
    { AttributeName: 'id', AttributeType: 'S' },
    { AttributeName: 'nbMembers', AttributeType: 'N' },
    { AttributeName: 'updateTime', AttributeType: 'N' }
  ],
  GlobalSecondaryIndexes: [{
    IndexName: 'updateMembers',
    KeySchema: [
      {
        AttributeName: 'nbMembers',
        KeyType: 'HASH'
      },
      {
        AttributeName: 'updateTime',
        KeyType: 'RANGE'
      }
    ],
    Projection: {
      ProjectionType: 'ALL'
    },
    ProvisionedThroughput: {
      ReadCapacityUnits: 5,
      WriteCapacityUnits: 5
    }
  }],
  ProvisionedThroughput: {
    ReadCapacityUnits: 5,
    WriteCapacityUnits: 5
  }
}