
const path = require('path')
const os = require('os')
const createError = require('http-errors')
const express = require('express')
const cookieParser = require('cookie-parser')
const morgan = require('morgan')
const BodyParser = require('body-parser')
const route4express = require('route4express')
const lout4express = require('lout4express')
const joi4express = require('joi4express')

let app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(morgan('dev'))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use(BodyParser.json())

// Building the routes using route4express
let routes = route4express(path.join(__dirname, '/routes/'))
routes = routes.map(route => {
  app[route.method](route.url, joi4express(route))

  return route
})

// Building the api documentation
app.all('/', lout4express(routes, os.hostname()))

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  logger.error('Uncaught error', err)
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
