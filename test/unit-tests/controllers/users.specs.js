const path = require('path')
require(path.join(process.cwd(), 'test', 'setup'))

const mockery = require('mockery')
const httpMocks = require('node-mocks-http')

const controllersPath = path.join(process.cwd(), 'controllers')

describe('Test - Controllers - Users', () => {
  let userController
  let userModelMock

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    })

    userModelMock = {
      getAll: sinon.stub()
    }

    mockery.registerMock(path.join(process.cwd(), 'models', 'users'), userModelMock)
    userController = require(path.join(controllersPath, 'users'))
  })

  afterEach(() => {
    mockery.deregisterAll()
    mockery.disable()
  })

  describe('Controller - Users - getUsers', () => {
    it('Success case', () => {
      let req = httpMocks.createRequest()
      let res = httpMocks.createResponse({
        eventEmitter: require('events').EventEmitter
      })
      const results = [{
        id: 1,
        name: 'test'
      }]
      userModelMock.getAll = sinon.stub().resolves(results)

      let prom = userController.getUsers(req, res)

      return prom.should.be.fulfilled
        .then(() => {
          expect(res.statusCode).to.equal(200)
          expect(JSON.parse(res._getData())).to.deep.equal({ success: true, users: results })
        })
    })

    it('Error case', () => {
      let req = httpMocks.createRequest()
      let res = httpMocks.createResponse({
        eventEmitter: require('events').EventEmitter
      })
      userModelMock.getAll = sinon.stub().rejects(new Error())

      let prom = userController.getUsers(req, res)

      return prom.should.be.fulfilled
        .then(() => {
          expect(res.statusCode).to.equal(520)
          expect(res._getData()).to.deep.equal({ success: false })
        })
    })
  })

  describe('Controller - Users - createUser', () => {
    it('Success case', () => {
      let req = httpMocks.createRequest()
      let res = httpMocks.createResponse({
        eventEmitter: require('events').EventEmitter
      })
      const results = {
        id: 1,
        name: 'test'
      }
      userModelMock.create = sinon.stub().resolves(results)

      let prom = userController.createUser(req, res)

      return prom.should.be.fulfilled
        .then(() => {
          expect(res.statusCode).to.equal(201)
          expect(JSON.parse(res._getData())).to.deep.equal({ success: true, user: results })
        })
    })

    it('Error case', () => {
      let req = httpMocks.createRequest()
      let res = httpMocks.createResponse({
        eventEmitter: require('events').EventEmitter
      })
      userModelMock.create = sinon.stub().rejects(new Error())

      let prom = userController.createUser(req, res)

      return prom.should.be.fulfilled
        .then(() => {
          expect(res.statusCode).to.equal(520)
        })
    })

    it('Error when user already exists', () => {
      let req = httpMocks.createRequest()
      let res = httpMocks.createResponse({
        eventEmitter: require('events').EventEmitter
      })
      let error = new Error()
      error.statusCode = 400
      error.code = 'ConditionalCheckFailedException'
      userModelMock.create = sinon.stub().rejects(error)

      let prom = userController.createUser(req, res)

      return prom.should.be.fulfilled
        .then(() => {
          expect(res.statusCode).to.equal(400)
          expect(JSON.parse(res._getData()).success).to.deep.equal(false)
        })
    })
  })
})
