
const Joi = require('joi')
const path = require('path')
require(path.join(process.cwd(), 'test', 'setup'))

const mockery = require('mockery')

const modelsPath = path.join(process.cwd(), 'models')
let userModel

describe('Test - Models - Users', () => {
  let scan
  let put

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    })

    scan = sinon.stub()
    let DocumentClient = function () {
      return {
        scan: () => {
          return {
            promise: scan
          }
        },
        put: () => {
          return {
            promise: put
          }
        }
      }
    }

    let awsMock = {
      DynamoDB: {
        DocumentClient
      }
    }
    mockery.registerMock('../modules/aws', awsMock)
    userModel = require(path.join(modelsPath, 'users'))
  })

  afterEach(() => {
    mockery.deregisterAll()
    mockery.disable()
  })

  describe('Models - Users - getExample', () => {
    it('Success case', () => {
      let result = userModel.getExample()

      expect(result).to.deep.equal({
        id: '602af7f6-67eb-4399-bf14-8191725fc194',
        groupId: 'default'
      })
    })
  })

  describe('Models - Users - getFormat', () => {
    it('Success case', () => {
      let result = userModel.getFormat()

      expect(Joi.object().keys(result).validate(userModel.getExample()).error).to.equal(null)
    })
  })

  describe('Models - Users - getAll', () => {
    it('Success case', () => {
      const items = [userModel.getExample()]
      scan = sinon.stub().resolves({ Items: items })
      let prom = userModel.getAll()

      return prom.should.be.fulfilled
        .then(result => {
          expect(result).to.deep.equal(items)
        })
    })

    it('Fail case', () => {
      const err = new Error('Fail case')
      scan = sinon.stub().rejects(err)
      let prom = userModel.getAll()

      return prom.should.be.rejected
        .then(error => {
          expect(error).to.equal(err)
        })
    })
  })

  describe('Models - Users - createUser', () => {
    it('Success case', () => {
      put = sinon.stub().resolves()
      let user = { userName: 'user' }
      let prom = userModel.create(user)

      return prom.should.be.fulfilled
        .then(result => {
          expect(result.id).to.be.a('string')
        })
    })

    it('Fail case', () => {
      const err = new Error('Fail case')
      let user = { userName: 'user' }
      put = sinon.stub().rejects(err)
      let prom = userModel.create(user)

      return prom.should.be.rejected
        .then(error => {
          expect(error).to.equal(err)
        })
    })
  })
})
