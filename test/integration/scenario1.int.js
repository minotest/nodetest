const path = require('path')
require(path.join(process.cwd(), 'test', 'setup'))

const mockery = require('mockery')
const request = require('supertest')

const migration = require(path.join(process.cwd(), 'bin', 'migration'))

let group = {
  description: 'group_lala_description'
}
let user1 = {}
let user2 = {}
let group1 = {}

describe.only('Route - Users', function () {
  let app

  before(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    })

    mockery.registerMock('./modules/logger-manager', logger)
    app = require(path.join(process.cwd(), 'app'))

    return migration.deleteTables()
      .then(() => migration.createTables())
  })

  after(() => {
    mockery.deregisterAll()
    mockery.disable()
  })

  describe('Successfull scenario', function () {
    it('Retrieve empty user list', function () {
      return request(app)
        .get('/v1/users')
        .expect(res => {
          expect(res.body.success).to.be.equal(true)
          expect(res.statusCode).to.be.equal(200)
        })
    })

    it('Create a user', function () {
      return request(app)
        .post('/v1/users')
        .expect(res => {
          expect(res.body).to.be.an('object')
          expect(res.statusCode).to.be.equal(201)
          user1 = res.body.user
        })
    })

    it('Create a group', function () {
      return request(app)
        .post('/v1/groups')
        .send(group)
        .expect(res => {
          expect(res.body).to.be.an('object')
          expect(res.body.group.description).to.be.deep.equal(group.description)
          expect(res.body.group.nbMembers).to.be.deep.equal(0)
          expect(res.statusCode).to.be.equal(201)
          group1 = res.body.group
        })
    })

    it('Join a group', function () {
      let groupId = {
        groupId: group1.id
      }

      return request(app)
        .put(`/v1/users/${user1.id}`)
        .send(groupId)
        .expect(res => {
          expect(res.body).to.be.an('object')
          expect(res.statusCode).to.be.equal(200)
        })
    })

    it('Retrieve user list', function () {
      return request(app)
        .get('/v1/users')
        .expect(res => {
          expect(res.body.success).to.be.equal(true)
          expect(res.statusCode).to.be.equal(200)
          expect(res.body.users[0].groupId).to.be.equal(group1.id)
        })
    })

    it('Creating second user', function () {
      return request(app)
        .post('/v1/users')
        .expect(res => {
          expect(res.body).to.be.an('object')
          expect(res.statusCode).to.be.equal(201)
          user2 = res.body.user
        })
    })

    it('Test group limit', function () {
      config.max_members = 1
      let groupId = {
        groupId: group1.id
      }

      return request(app)
        .put(`/v1/users/${user2.id}`)
        .send(groupId)
        .expect(res => {
          expect(res.body).to.be.an('object')
          expect(res.statusCode).to.be.equal(400)
        })
    })

    it('Adding the second user to the group', function () {
      config.max_members = 2
      let groupId = {
        groupId: group1.id
      }

      return request(app)
        .put(`/v1/users/${user2.id}`)
        .send(groupId)
        .expect(res => {
          expect(res.body).to.be.an('object')
          expect(res.statusCode).to.be.equal(200)
        })
    })

    it('Leave a group', function () {
      let groupId = {
        groupId: group1.id
      }

      return request(app)
        .delete(`/v1/users/${user1.id}`)
        .send(groupId)
        .expect(res => {
          expect(res.body).to.be.an('object')
          expect(res.statusCode).to.be.equal(200)
        })
    })

    it('Re-Adding first user to the group', function () {
      let groupId = {
        groupId: group1.id
      }

      return request(app)
        .put(`/v1/users/${user1.id}`)
        .send(groupId)
        .expect(res => {
          expect(res.body).to.be.an('object')
          expect(res.statusCode).to.be.equal(200)
        })
    })

    it('Retrieving group - group is full', function () {
      let expectedResult = {
        group: {
          description: 'group_lala_description',
          id: group1.id,
          nbMembers: 2,
          isFull: 'true'
        },
        members: [ user1.id, user2.id ],
        success: true
      }

      return request(app)
        .get(`/v1/groups/${group1.id}`)
        .expect(res => {
          expect(res.body).to.be.an('object')
          expect(res.body.success).to.deep.equal(expectedResult.success)
          expect(res.body.group).to.deep.equal(expectedResult.group)
          expect(res.body.members.length).to.deep.equal(expectedResult.members.length)
          expect(res.statusCode).to.be.equal(200)
        })
    })

    it('Not retrieving recommended groups when isFull has been set to true', function () {
      return request(app)
        .get(`/v1/groups`)
        .expect(res => {
          expect(res.body).to.be.an('object')
          expect(res.body.groups.length).to.be.equal(0)
          expect(res.statusCode).to.be.equal(200)
        })
    })

    it('Leaving a group to have a group not full', function () {
      let groupId = {
        groupId: group1.id
      }

      return request(app)
        .delete(`/v1/users/${user1.id}`)
        .send(groupId)
        .expect(res => {
          expect(res.body).to.be.an('object')
          expect(res.statusCode).to.be.equal(200)
        })
    })

    it('Retrieving recommended groups when nbMembers > max', function () {
      return request(app)
        .get(`/v1/groups`)
        .expect(res => {
          expect(res.body).to.be.an('object')
          expect(res.body.groups.length).to.be.equal(1)
          expect(res.statusCode).to.be.equal(200)
        })
    })
  })
})
