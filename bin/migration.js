/** Try to create the tables in dynamodb **/

const AWS = require('../modules/aws')
const dynamodb = new AWS.DynamoDB()

function createTableUsers () {
  let params = {
    TableName: 'users',
    KeySchema: [
      { AttributeName: 'id', KeyType: 'HASH' } // Partition key
    ],
    AttributeDefinitions: [
      { AttributeName: 'id', AttributeType: 'S' },
      { AttributeName: 'groupId', AttributeType: 'S' }
    ],
    GlobalSecondaryIndexes: [{
      IndexName: 'groupId',
      KeySchema: [
        {
          AttributeName: 'groupId',
          KeyType: 'HASH'
        }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 5,
        WriteCapacityUnits: 5
      }
    }],
    ProvisionedThroughput: {
      ReadCapacityUnits: 5,
      WriteCapacityUnits: 5
    }
  }

  logger.debug('Creating table Users')

  return dynamodb.createTable(params).promise()
    .then(data => {
      logger.info('Created table. Table description JSON:', JSON.stringify(data, null, 2))
    })
    .catch(err => {
      logger.error('Unable to create table, Error JSON:', JSON.stringify(err, null, 2))
      return Promise.resolve()
    })
}

function createTableGroups () {
  let params = {
    TableName: 'groups',
    KeySchema: [
      { AttributeName: 'id', KeyType: 'HASH' } // Partition key
    ],
    AttributeDefinitions: [
      { AttributeName: 'id', AttributeType: 'S' },
      { AttributeName: 'isFull', AttributeType: 'S' },
      { AttributeName: 'updateTime', AttributeType: 'N' }
    ],
    GlobalSecondaryIndexes: [{
      IndexName: 'updateMembers',
      KeySchema: [
        {
          AttributeName: 'isFull',
          KeyType: 'HASH'
        },
        {
          AttributeName: 'updateTime',
          KeyType: 'RANGE'
        }
      ],
      Projection: {
        ProjectionType: 'ALL'
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 5,
        WriteCapacityUnits: 5
      }
    }],
    ProvisionedThroughput: {
      ReadCapacityUnits: 5,
      WriteCapacityUnits: 5
    }
  }

  logger.debug('Creating table Groups')

  return dynamodb.createTable(params).promise()
    .then(data => {
      logger.info('Created table. Table description JSON:', JSON.stringify(data, null, 2))
    })
    .catch(err => {
      logger.error('Unable to create table, Error JSON:', JSON.stringify(err, null, 2))
      return Promise.resolve()
    })
}

function createTables () {
  return Promise.all([
    createTableUsers(),
    createTableGroups()
  ])
    .catch(err => {
      logger.error('createTables - Issue while creating tables', err)
      return Promise.resolve()
    })
}

function deleteTableUsers () {
  let params = {
    TableName: 'users'
  }

  return dynamodb.deleteTable(params).promise()
    .then(data => {
      logger.info('Table Deleted table. Table description JSON:', JSON.stringify(data, null, 2))
    })
    .catch(err => {
      logger.error('Unable to delete table, Error JSON:', JSON.stringify(err, null, 2))
      return Promise.resolve()
    })
}

function deleteTableGroups () {
  let params = {
    TableName: 'groups'
  }

  return dynamodb.deleteTable(params).promise()
    .then(data => {
      logger.info('Table Deleted table. Table description JSON:', JSON.stringify(data, null, 2))
    })
    .catch(err => {
      logger.error('Unable to delete table, Error JSON:', JSON.stringify(err, null, 2))
      return Promise.resolve()
    })
}

function deleteTables () {
  return Promise.all([
    deleteTableUsers(),
    deleteTableGroups()
  ])
    .catch(err => {
      logger.error('deleteTables - Issue while deleting tables', err)
      return Promise.resolve()
    })
}

module.exports = {
  createTables,
  deleteTables
}
