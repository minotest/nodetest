const Promise = require('bluebird')
const Joi = require('joi')
const uuidV5 = require('uuid/v5')
const uuidV4 = require('uuid/v4')

const AWS = require('../modules/aws')
const dynamoClient = new AWS.DynamoDB.DocumentClient()

const tableName = 'groups'
const indexName = 'updateMembers'

/**
  * Return a sample group object
  *
  * @return {Object} return a sample group Object
  */
function getExample () {
  return {
    id: '602af7f6-67eb-4399-bf14-8191725fc194',
    nbMembers: 0,
    description: 'description',
    isFull: 'false'
  }
}

/**
  * Return the Joi.format for a group
  *
  * @return {Object} Joi group format
  */
function getFormat () {
  return {
    id: Joi.string().required(),
    nbMembers: Joi.number().optional(),
    description: Joi.string().required(),
    isFull: Joi.string().optional()
  }
}

/**
  * Retrieve the groups which have been updated the most recently
  *
  * @return {Array[groups]} return an array of groups
  */
function getRecommendedGroups () {
  let query = {
    TableName: tableName,
    IndexName: indexName,
    ProjectionExpression: 'id, description, nbMembers',
    KeyConditionExpression: '#k_isFull = :notFull',
    ExpressionAttributeValues: { ':notFull': 'false' },
    ExpressionAttributeNames: { '#k_isFull': 'isFull' },
    ScanIndexForward: false,
    Limit: 10
  }

  logger.debug('model.group.getRecommendedGroups - querying the database')
  return dynamoClient.query(query).promise()

    .then(result => {
      logger.debug('model.group.getRecommendedGroups - Groups successfully retrieved', result)
      return result.Items
    })

    .catch(error => {
      logger.error('model.group.getRecommendedGroups - Could not retrieve the groups', error)
      return Promise.reject(error)
    })
}

/**
  * Return a group from the database
  *
  * @return {Object} return a group
  */
function getGroup (groupId) {
  let query = {
    TableName: tableName,
    Key: { 'id': groupId }
  }

  logger.debug('model.group.getGroup - querying the database')
  return dynamoClient.get(query).promise()

    .then(result => {
      logger.debug('model.group.getGroup - Group successfully retrieved', result)
      return result.Item
    })

    .catch(error => {
      logger.error('model.group.getGroup - Could not retrieve the group', error)
      return Promise.reject(error)
    })
}

/**
  * Create a group in database
  *
  * @return {Promise(Object)} return a group
  */
function create (group) {
  group.id = uuidV5(config.uuid_namespace, uuidV4())
  group.nbMembers = 0

  let query = {
    TableName: tableName,
    Item: group
  }
  return dynamoClient.put(query).promise()

    .then(result => {
      logger.debug('model.group.create - Group successfully created', result)
      return group
    })

    .catch(error => {
      logger.error('model.group.create - Could not create the group', group)
      return Promise.reject(error)
    })
}

/**
  * Add a member to the group
  * @param {string} groupId id of the group to add a member to
  * @return {Promise()} return nothing
  */
function addMember (groupId, isFull) {
  let query = {
    Update: {
      TableName: tableName,
      Key: { 'id': { S: groupId } },
      UpdateExpression: 'ADD #k_nb_member :nb SET updateTime = :latestTime, isFull = :status',
      ExpressionAttributeValues: {
        ':nb': { N: '1' },
        ':max': { N: config.max_members.toString() },
        ':latestTime': { N: Date.now().toString() },
        ':status': { S: isFull }
      },
      ExpressionAttributeNames: {
        '#k_nb_member': 'nbMembers'
      },
      ConditionExpression: ':max > #k_nb_member'
    }
  }

  return query
}

/**
  * Add a member to the group
  *
  * @param {string} groupId id of the group to remove a member from
  * @return {Promise()} return nothing
  */
function removeMember (groupId) {
  let query = {
    Update: {
      TableName: tableName,
      Key: { 'id': { S: groupId } },
      UpdateExpression: 'ADD #k_nb_member :nb SET isFull = :status',
      ExpressionAttributeValues: {
        ':nb': { N: '-1' },
        ':status': { S: 'false' }
      },
      ExpressionAttributeNames: {
        '#k_nb_member': 'nbMembers'
      }
    }
  }

  return query
}

module.exports = {
  getExample,
  getFormat,
  getRecommendedGroups,
  getGroup,
  create,
  addMember,
  removeMember
}
