const Promise = require('bluebird')
const Joi = require('joi')
const uuidV5 = require('uuid/v5')
const uuidV4 = require('uuid/v4')

const AWS = require('../modules/aws')
const dynamoClient = new AWS.DynamoDB.DocumentClient()

const tableName = 'users'
const indexName = 'groupId'

/**
  * Return a sample user object
  *
  * @return {Object} return a sample user Object
  */
function getExample () {
  return {
    id: '602af7f6-67eb-4399-bf14-8191725fc194',
    groupId: 'default'
  }
}

/**
  * Return the Joi.format for a user
  *
  * @return {Object} Joi user format
  */
function getFormat () {
  return {
    id: Joi.string().required(),
    groupId: Joi.string().required()
  }
}

/**
  * Return all the users in database
  *
  * @return {Array[users]} return an array of users
  */
function getAll () {
  let query = {
    TableName: tableName
  }

  logger.debug('model.user.getAll - querying the database')
  return dynamoClient.scan(query).promise()

    .then(result => {
      logger.debug('model.user.getAll - User successfully retrieved', result)
      return result.Items
    })

    .catch(error => {
      logger.error('model.user.getAll - Could not retrieve the users', error)
      return Promise.reject(error)
    })
}

/**
  * Create a user in database
  *
  * @return {Object} return a user
  */
function create (user) {
  user.id = uuidV5(config.uuid_namespace, uuidV4())
  user.groupId = 'default'

  let query = {
    TableName: tableName,
    Item: user
  }
  return dynamoClient.put(query).promise()

    .then(result => {
      logger.debug('model.user.create - User successfully created', result)
      return user
    })

    .catch(error => {
      logger.error('model.user.create - Could not create the user', user)
      return Promise.reject(error)
    })
}

/**
  * Return all the users in database
  *
  * @return {Array[users]} return an array of users
  */
function getUserByGroup (groupId) {
  let query = {
    TableName: tableName,
    IndexName: indexName,
    KeyConditionExpression: 'groupId = :groupId',
    ExpressionAttributeValues: { ':groupId': groupId }
  }

  logger.debug('model.user.getUser - querying the database')
  return dynamoClient.query(query).promise()

    .then(result => {
      logger.debug('model.user.getUser - User successfully retrieved', result)
      return result.Items
    })

    .catch(error => {
      logger.error('model.user.getUser - Could not retrieve the user', error)
      return Promise.reject(error)
    })
}

/**
  * Return a specific user from the database
  *
  * @return {Object} return a user
  */
function getUser (userId) {
  let query = {
    TableName: tableName,
    Key: { 'id': userId }
  }

  logger.debug('model.user.getUser - querying the database')
  return dynamoClient.get(query).promise()

    .then(result => {
      logger.debug('model.user.getUser - User successfully retrieved', result)
      return result.Item
    })

    .catch(error => {
      logger.error('model.user.getUser - Could not retrieve the user', error)
      return Promise.reject(error)
    })
}

/**
  * Add a member to a group
  *
  * @param {string} userId user id of the user who is joining a group
  * @param {string} groupId group id of the user who is joining a group
  * @return {Promise(Object)} returns the updated user
  */
function joinGroup (userId, groupId) {
  let query = {
    Update: {
      TableName: tableName,
      Key: { 'id': { S: userId } },
      UpdateExpression: 'SET groupId = :aGroupId',
      ExpressionAttributeValues: { ':aGroupId': { S: groupId } }
    }
  }

  return query
}

/**
  * Set the group of the user to default
  *
  * @param {string} userId user id of the user who is joining a group
  * @return {Promise(Object)} returns the updated user
  */
function leaveGroup (userId) {
  let query = {
    Update: {
      TableName: tableName,
      Key: { 'id': { S: userId } },
      UpdateExpression: 'SET groupId = :aGroupId',
      ExpressionAttributeValues: { ':aGroupId': { S: 'default' } }
    }
  }

  return query
}

module.exports = {
  getExample,
  getFormat,
  getAll,
  create,
  joinGroup,
  leaveGroup,
  getUser,
  getUserByGroup
}
