
const Joi = require('joi')
const path = require('path')

const controllersPath = path.join(process.cwd(), 'controllers')
const modelsPath = path.join(process.cwd(), 'models')

const userModel = require(path.join(modelsPath, 'users'))
const userFormat = userModel.getFormat()
const userExample = userModel.getExample()

const listResponseFormat = Joi.object().keys({
  users: Joi.array().items(Joi.object().keys(userFormat)),
  success: Joi.boolean().required()
})
const listResponseExample = {
  users: [userExample],
  success: true
}

const createResponseFormat = Joi.object().keys({
  user: Joi.object().keys(userFormat),
  success: Joi.boolean().required()
})
const createResponseExample = {
  user: userExample,
  success: true
}

module.exports = [
  {
    method: 'get',
    url: '/users',
    handler: require(path.join(controllersPath, 'users')).getUsers,
    description: 'Get all users',
    validate: {},
    response: {
      status: {
        200: listResponseFormat.description('A list of user items').example(listResponseExample),
        400: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Request format not valid'),
        520: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Unexpected handled error'),
        500: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Unexpected unhandled server error')
      }
    },
    tags: ['users']
  },
  {
    method: 'post',
    url: '/users',
    handler: require(path.join(controllersPath, 'users')).createUser,
    description: 'Create a user',
    validate: {
    },
    response: {
      status: {
        201: createResponseFormat.description('The user created').example(createResponseExample),
        400: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Request format not valid'),
        520: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Unexpected handled error'),
        500: Joi.any().description('Unexpected unhandled server error')
      }
    },
    tags: ['users']
  },
  {
    method: 'put',
    url: '/users/:userId',
    handler: require(path.join(controllersPath, 'users')).joinGroup,
    description: 'Make a user join a group',
    validate: {
      params: {
        userId: Joi.string().required().description('User id of the user who wants to join a group').example('602af7f6-67eb-4399-bf14-8191725fc194')
      },
      body: {
        groupId: Joi.string().required().description('Specify the group id the user want to join').example('602af7f6-67eb-4399-bf14-8191725fc194')
      }
    },
    response: {
      status: {
        200: Joi.object().keys({
          success: Joi.boolean().required()
        }),
        400: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Request format not valid'),
        520: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Unexpected handled error'),
        500: Joi.any().description('Unexpected unhandled server error')
      }
    },
    tags: ['users']
  },
  {
    method: 'delete',
    url: '/users/:userId',
    handler: require(path.join(controllersPath, 'users')).leaveGroup,
    description: 'Make a user leave his current group',
    validate: {
      params: {
        userId: Joi.string().required().description('User id of the user who wants to leave a group').example('602af7f6-67eb-4399-bf14-8191725fc194')
      }
    },
    response: {
      status: {
        200: Joi.object().keys({
          success: Joi.boolean().required()
        }),
        400: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Request format not valid'),
        520: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Unexpected handled error'),
        500: Joi.any().description('Unexpected unhandled server error')
      }
    },
    tags: ['users']
  }
]
