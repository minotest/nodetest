
const Joi = require('joi')
const path = require('path')
const _ = require('lodash')

const controllersPath = path.join(process.cwd(), 'controllers')
const modelsPath = path.join(process.cwd(), 'models')

const groupModel = require(path.join(modelsPath, 'groups'))
const groupFormat = groupModel.getFormat()
const groupExample = groupModel.getExample()

const listResponseFormat = Joi.object().keys({
  groups: Joi.array().items(Joi.object().keys(groupFormat)),
  success: Joi.boolean().required()
})
const listResponseExample = {
  groups: [groupExample],
  success: true
}
const getResponseFormat = Joi.object().keys({
  group: Joi.object().keys(groupFormat).required(),
  members: Joi.array().items(Joi.string()).required(),
  success: Joi.boolean().required()
})
const getResponseExample = {
  group: [groupExample],
  members: ['602af7f6-67eb-4399-bf14-8191725fc194'],
  success: true
}
const createGroupFormat = Joi.object().keys(_.omit(groupFormat, ['id', 'nbMembers']))
const createGroupExample = _.omit(groupExample, ['id', 'nbMembers'])
const createResponseFormat = Joi.object().keys({
  group: Joi.object().keys(groupFormat).required(),
  success: Joi.boolean().required()
})
const createResponseExample = {
  group: groupExample,
  success: true
}

module.exports = [
  {
    method: 'get',
    url: '/groups',
    handler: require(path.join(controllersPath, 'groups')).getRecommendedGroups,
    description: 'Get recommended groups based on updatedTime, groups which are full won t be shown',
    validate: {},
    response: {
      status: {
        200: listResponseFormat.description('A list of group items').example(listResponseExample),
        400: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Request format not valid'),
        520: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Unexpected handled error'),
        500: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Unexpected unhandled server error')
      }
    },
    tags: ['groups']
  },
  {
    method: 'get',
    url: '/groups/:groupId',
    handler: require(path.join(controllersPath, 'groups')).getGroup,
    description: 'Get a group and its members',
    validate: {
      params: {
        groupId: Joi.string().required().description('Group id').example('602af7f6-67eb-4399-bf14-8191725fc194')
      }
    },
    response: {
      status: {
        200: getResponseFormat.description('A list of group items').example(getResponseExample),
        400: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Request format not valid'),
        520: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Unexpected handled error'),
        500: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Unexpected unhandled server error')
      }
    },
    tags: ['groups']
  },
  {
    method: 'post',
    url: '/groups',
    handler: require(path.join(controllersPath, 'groups')).createGroup,
    description: 'Create a group',
    validate: {
      body: createGroupFormat.example(createGroupExample)
    },
    response: {
      status: {
        201: createResponseFormat.description('The group created').example(createResponseExample),
        400: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Request format not valid'),
        520: Joi.object().keys({
          statusCode: Joi.number(),
          error: Joi.any(),
          message: Joi.string(),
          success: Joi.boolean()
        }).description('Unexpected handled error'),
        500: Joi.any().description('Unexpected unhandled server error')
      }
    },
    tags: ['groups']
  }
]
