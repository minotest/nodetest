
const AWS = require('./aws')
const dynamoClient = new AWS.DynamoDB()

function executeTransaction (queries) {
  return dynamoClient.transactWriteItems({
    TransactItems: queries
  }).promise()

    .then(result => {
      logger.debug('modules.dynamo-transaction.executeTransaction - Transasction success ', result)
      return result
    })

    .catch(error => {
      logger.error('modules.dynamo-transaction.executeTransaction - Error while executing queries', error)
      return Promise.reject(error)
    })
}

module.exports = {
  executeTransaction
}
