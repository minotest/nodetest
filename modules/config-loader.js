'use strict'

const path = require('path')

const env = process.env.NODE_ENV || 'dev'

const configFile = path.join(process.cwd(), 'config', env)
let config = require(configFile)

const processEnv = process.env || {}

// Replacing config values with environment variables
Object.keys(processEnv).forEach(key => {
  if (config[key]) {
    config[key] = processEnv[key]
  }
})

logger.debug('Loading \'%s\' configuration.', configFile)

module.exports = config
