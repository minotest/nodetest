'use strict'

const path = require('path')
const winstonConfigMonitor = require('winston-config-monitor')
const winston = require('winston')

let configFile = path.join(process.cwd(), 'config', 'winston-options.json')
let logOptions = require(configFile)

let logger = winstonConfigMonitor.getLogger()

logger.exitOnError = true

winstonConfigMonitor.clear()

// Load all the logger options
for (let transportName in logOptions.loggersOptions) {
  winstonConfigMonitor.add(winston.transports[transportName], configFile, 'loggersOptions.' + transportName)
}

module.exports = logger
