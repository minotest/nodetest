/** Create the dynamoDb object to be used to query the database **/

const AWS = require('aws-sdk')

AWS.config.setPromisesDependency(require('bluebird'))

const awsConfig = {
  region: config.aws_region,
  accessKeyId: config.aws_access_key_id,
  secretAccessKey: config.aws_secret_access_key,
  endpoint: config.aws_dynamo_endpoint
}

AWS.config.update(awsConfig)

module.exports = AWS
