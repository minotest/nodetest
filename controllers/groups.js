const Promise = require('bluebird')
const path = require('path')
const _ = require('lodash')

const modelsPath = path.join(process.cwd(), 'models')
const groupModel = require(path.join(modelsPath, 'groups'))
const userModel = require(path.join(modelsPath, 'users'))

/**
  * Retrieve a list of groups
  *
  * @param {Object} req express request object
  * @param {Object} res express response object
  * @return {Promise} return a promise, send the response via res.send()
  */
function getRecommendedGroups (req, res) {
  return Promise.try(() => groupModel.getRecommendedGroups())

    .then(groupList => {
      logger.debug('controller.group.getRecommendedGroups - Groups successfully retrieved', groupList)
      return res.status(200).json({ success: true, groups: groupList })
    })

    .catch(error => {
      logger.error('controller.group.getRecommendedGroups - Issue while retrieving the groups', error)
      return res.status(520).send({ success: false })
    })
}

/**
  * Get a group and its members
  *
  * @param {Object} req express request object
  * @param {Object} res express response object
  * @return {Promise} return a promise, send the response via res.send()
  */
function getGroup (req, res) {
  return Promise.try(() => {
    // Retrieve the group and the members of a group
    return Promise.join(
      groupModel.getGroup(req.params.groupId),
      userModel.getUserByGroup(req.params.groupId),
      (group, users) => {
        return {
          group: _.omit(group, 'updateTime'),
          members: users.map(user => user.id)
        }
      })
  })

    .then(result => {
      logger.debug('controller.group.getGroup - Groups successfully retrieved', result)
      result.success = true
      return res.status(200).json(result)
    })

    .catch(error => {
      logger.error('controller.group.getGroup - Issue while retrieving the groups', error)
      return res.status(520).send({ success: false })
    })
}

/**
  * Create a group
  *
  * @param {Object} req express request object
  * @param {Object} res express response object
  * @return {Promise} return a promise, send the response via res.send()
  */
function createGroup (req, res) {
  return Promise.try(() => groupModel.create(req.body))

    .then(group => {
      logger.debug('controller.group.create - Group successfully created', group)
      return res.status(201).json({ success: true, group })
    })

    .catch(error => {
      logger.error('controller.group.create - Issue while creating the group', error)
      if (error.statusCode === 400 && error.code === 'ConditionalCheckFailedException') {
        return res.status(400).json({ message: 'Group already exists', success: false })
      } else {
        return res.status(520).send({ success: false })
      }
    })
}

module.exports = {
  getRecommendedGroups,
  getGroup,
  createGroup
}
