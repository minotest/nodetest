const Promise = require('bluebird')
const path = require('path')

const modelsPath = path.join(process.cwd(), 'models')
const userModel = require(path.join(modelsPath, 'users'))
const groupModel = require(path.join(modelsPath, 'groups'))
const modulesPath = path.join(process.cwd(), 'modules')
const dynamoTransaction = require(path.join(modulesPath, 'dynamo-transaction'))

/**
  * Retrieve a list of users
  *
  * @param {Object} req express request object
  * @param {Object} res express response object
  * @return {Promise} return a promise, send the response via res.send()
  */
function getUsers (req, res) {
  return Promise.try(() => userModel.getAll())

    .then(userList => {
      logger.debug('controller.user.getUsers - Users successfully retrieved', userList)
      return res.status(200).json({ success: true, users: userList })
    })

    .catch(error => {
      logger.error('controller.user.getUsers - Issue while retrieving the users', error)
      return res.status(520).send({ success: false })
    })
}

/**
  * Create a user
  *
  * @param {Object} req express request object
  * @param {Object} res express response object
  * @return {Promise} return a promise, send the response via res.send()
  */
function createUser (req, res) {
  return Promise.try(() => userModel.create(req.body))

    .then(user => {
      logger.debug('controller.user.create - User successfully created', user)
      return res.status(201).json({ success: true, user })
    })

    .catch(error => {
      logger.error('controller.user.create - Issue while creating the user', error)
      if (error.statusCode === 400 && error.code === 'ConditionalCheckFailedException') {
        return res.status(400).json({ message: 'User already exists', success: false })
      } else {
        return res.status(520).send({ success: false })
      }
    })
}

/**
  * Make a user join a group and increase the group number of members
  *
  * @param {Object} req express request object
  * @param {Object} res express response object
  * @return {Promise} return a promise, send the response via res.send()
  */
function joinGroup (req, res) {
  return Promise.try(() => {
    return groupModel.getGroup(req.body.groupId)

      .tap(group => {
        if (group.nbMembers === config.max_members) {
          // If the group is full the user can't join the group
          let error = new Error('Group is full')
          error.statusCode = 400

          return Promise.reject(error)
        }
      })

      .then(group => {
        // Determine if the group is gonna be full after adding the user
        // As we're about to add a user the nb members is gonna be incremented
        // isFull is a string as it becomes the hash key of the GSI
        let isFull = 'false'
        if (group.nbMembers >= (config.max_members - 1)) {
          isFull = 'true'
        }

        // Prepare the queries to add the user to a group
        const query = [
          groupModel.addMember(req.body.groupId, isFull),
          userModel.joinGroup(req.params.userId, req.body.groupId)
        ]

        return dynamoTransaction.executeTransaction(query)
      })

      .then(user => {
        logger.debug('controller.user.joinGroup - User successfully joined group', user)
        return res.status(200).json({ success: true })
      })
  })

    .catch(error => {
      logger.error('controller.user.joinGroup - Issue while joining a group', error)
      if (error.statusCode === 400) {
        return res.status(400).json({ message: 'Group is full', success: false })
      } else {
        return res.status(520).send({ success: false })
      }
    })
}

/**
  * Make a user leave his current group and decrease the group number of members
  *
  * @param {Object} req express request object
  * @param {Object} res express response object
  * @return {Promise} return a promise, send the response via res.send()
  */
function leaveGroup (req, res) {
  return Promise.try(() => {
    // Retrieve the user and verify that he exists
    return userModel.getUser(req.params.userId)

      .then(user => {
        // Prepare the queries to make the user leave the group
        const query = [
          userModel.leaveGroup(req.params.userId),
          groupModel.removeMember(user.groupId)
        ]

        return dynamoTransaction.executeTransaction(query)
      })

      .then(user => {
        logger.debug('controller.user.leaveGroup - User successfully joined group', user)
        return res.status(200).json({ success: true })
      })
  })

    .catch(error => {
      logger.error('controller.user.leaveGroup - Issue while leaving a group', error)
      if (error.statusCode === 400 && error.message.indexOf('ConditionalCheckFailedException')) {
        return res.status(400).json({ message: 'Condition check fail', success: false })
      } else {
        return res.status(520).send({ success: false })
      }
    })
}

module.exports = {
  getUsers,
  createUser,
  joinGroup,
  leaveGroup
}
